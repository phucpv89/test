## Task Description

*provide description of current problems/issues*

## Task tracking

* [ ] Description 1
* [ ] Description 2

## Additional Information

* App Version: [FILL THIS OUT:Version ZaloPay]
* Platform: [FILL THIS OUT: iOS, Android, or both?]
* Environment: [FILL THIS OUT: Sandbox or Staging or Production]

/label ~RequestTest
