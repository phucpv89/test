Please read the following carefully before opening a new issue.

We use Gitlab Issues for tracking bugs in Zalo Pay projects.

- If you have a non-bug question, ask on Slack

Your issue may be closed without explanation if it does not provide the information required by this template.

--- Please use this template, and delete everything above this line before submitting your issue --- 

### Description

[FILL THIS OUT: Explain what you did, what you expected to happen, and what actually happens.]

### Reproduction

[FILL THIS OUT: Try to reproduce your bug on rnplay.org and provide a link. If you can't reproduce the bug on rnplay.org, provide a sample project.]

### Solution

[FILL THIS OUT: What needs to be done to address this issue? Ideally, provide a pull request with a fix.]

### Additional Information

* App version: [FILL THIS OUT: Does the bug reproduce on the latest Zalo Pay release?]
* Platform: [FILL THIS OUT: iOS, Android, or both?]
* Operating System: [FILL THIS OUT: MacOS, Linux, or Windows?]
* Dev tools: [FILL THIS OUT: Xcode or Android Studio version, iOS or Android SDK version, if applicable]
* Build configuration: [FILL THIS OUT: Debug, Release, or both]
* Deploy Environment: [FILL THIS OUT: Sandbox, Staging, Production, or any combination]


/label ~Bugs
